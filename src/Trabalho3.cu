/*
 *  Trabalho3.cu
 *
 *  Jonatas Lopes de Paiva
 *  Marcos Sandim
 */

#define BLOCK_SIZE_X  32
#define BLOCK_SIZE_Y  32


#include <stdio.h>
#include <iostream>
#include <time.h>
#include <sstream>
#include <assert.h>
#include "operations2.h"
#include <cuda_profiler_api.h>

#define checkCuda(x) checkCudaImp(x, __FILE__, __LINE__)

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
inline
cudaError_t checkCudaImp(cudaError_t result, const char* file, int line)
{
//#if defined(DEBUG) || defined(_DEBUG)
	if (result != cudaSuccess)
	{
		fprintf(stderr, "%s(%d) CUDA Runtime Error: %s\n", file, line, cudaGetErrorString(result));
		assert(result == cudaSuccess);
	}
//#endif
	return result;
}

/**
 * Funcao que realiza a filtragem em si.
 */
__global__ void d_avgFilterRGB(unsigned char* input, unsigned char* output, int mode, unsigned int width, unsigned int height)
{
	// Posicao da thread atual dentro do grid
	long long x = threadIdx.x + blockIdx.x * blockDim.x; // width
	long long y = threadIdx.y + blockIdx.y * blockDim.y; // height

	// Confere para que so execute em blocos dentro da imagem
	if (x < width && y < height)
	{
		// Posicao de cada thread em uma dimensao
		long long globalIdx = y * width + x;

		__shared__ float sharedArr[BLOCK_SIZE_X+4][BLOCK_SIZE_Y+4];

		
		sharedArr[threadIdx.y+2][threadIdx.x+2] = ((float)input[globalIdx]);

		// Conversao para signed e possibilitar a comparacao com menor que 0
		int threadX = (int)threadIdx.x;
		int threadY = (int)threadIdx.y;

		// bordas da imagem
		if(x - 2 < 0)
		{
			sharedArr[threadIdx.y + 2][threadIdx.x] = 0.0;
			
			// Cantos de cada bloco 
			if(threadY - 2 < 0 )
			{
				sharedArr[threadIdx.y][threadIdx.x] = 0.0;
			}
			else if (threadY + 2 > blockDim.y - 1)
			{
				sharedArr[threadIdx.y + 4][threadIdx.x] = 0.0;
			}
		}
		else if (x + 2 > width - 1)
		{
			sharedArr[threadIdx.y + 2][threadIdx.x + 4] = 0.0;
			if(threadY - 2 < 0 )
			{
				sharedArr[threadIdx.y][threadIdx.x+4] = 0.0;
			}
			else if (threadY + 2 > blockDim.y - 1)
			{
				sharedArr[threadIdx.y + 4][threadIdx.x+4] = 0.0;
			}
		}
		
		// Bordas verticais da imagem
		if(y - 2 < 0)
		{
			sharedArr[threadIdx.y][threadIdx.x + 2] = 0.0;
			if(threadX - 2 < 0 )
			{
				sharedArr[threadIdx.y][threadIdx.x] = 0.0;
			}
			else if (threadX + 2 > blockDim.x - 1)
			{
				sharedArr[threadIdx.y][threadIdx.x + 4] = 0.0;
			}
		}
		else if(y + 2 > height - 1)
		{
			sharedArr[threadIdx.y + 4][threadIdx.x + 2] = 0.0;
			if(threadX - 2 < 0)
			{
				sharedArr[threadIdx.y + 4][threadIdx.x] = 0.0;
			}
			else if (threadX + 2 > blockDim.x - 1)
			{
				sharedArr[threadIdx.y + 4][threadIdx.x + 4] = 0.0;
			}
		}

		// bordas dos blocos que nao sao bordas da imagem
		if(threadX - 2 < 0 && x - 2 >= 0)
		{
			sharedArr[threadIdx.y+2][threadIdx.x] = input[y*width + (x-2)];

			// mesma logica, cantos dos blocos
			if(threadY - 2 < 0 && y - 2 >= 0)
			{
				sharedArr[threadIdx.y][threadIdx.x] = input[(y-2)*width + (x-2)];
			}
			else if (threadY + 2 > blockDim.y-1 && y + 2 < height)
			{
				sharedArr[threadIdx.y + 4][threadIdx.x] = input[(y+2)*width + (x-2)];
			}
		}
		else if(threadX + 2 > blockDim.x - 1 && x + 2 < width)
		{
			sharedArr[threadIdx.y+2][threadIdx.x + 4] = input[y*width + (x+2)];
			if(threadY - 2 < 0 && y - 2 >= 0)
			{
				sharedArr[threadIdx.y][threadIdx.x] = input[(y-2)*width + (x+2)];
			}
			else if (threadY + 2 > blockDim.y-1 && y + 2 < height)
			{
				sharedArr[threadIdx.y + 4][threadIdx.x] = input[(y+2)*width + (x+2)];
			}
		}

		// eixo y
		if(threadY - 2 < 0 && y - 2 >= 0)
		{
			sharedArr[threadIdx.y ][threadIdx.x+2] = input[(y-2)*width + x];
			if(threadX - 2 < 0 && x - 2 >= 0)
			{
				sharedArr[threadIdx.y][threadIdx.x] = input[(y-2)*width + (x-2)];
			}
			else if(threadX + 2 > blockDim.x - 1 && x + 2 < width)
			{
				sharedArr[threadIdx.y][threadIdx.x + 4] = input[(y-2)*width + (x+2)];
			}
		}
		else if(threadY + 2 > blockDim.y-1 && y + 2 < height)
		{
			sharedArr[threadIdx.y+4][threadIdx.x + 2] = input[(y+2)*width + x];
			if(threadX - 2 < 0 && x - 2 >= 0)
			{
				sharedArr[threadIdx.y + 4][threadIdx.x] = input[(y+2)*width + (x-2)];
			}
			else if(threadX + 2 > blockDim.x - 1 && x + 2 < width)
			{
				sharedArr[threadIdx.y + 4][threadIdx.x + 4] = input[(y+2)*width + (x+2)];
			}
		}

		__syncthreads();

		float sum = 0.0;

		int i, j;

		// calcula a media entre os pixels vizinhos para cada thread e coloca no output
		for(i = -2; i <=2 ; i++)
		{
			for(j = -2 ; j <= 2; j++)
			{
				sum += sharedArr[threadIdx.y + 2 + j][threadIdx.x + 2 + i] * 0.04;
			}
		}
		output[globalIdx] = ((unsigned char)sum);
	}
}

int main(int argc, char** argv)
{

	// Comeca a calcular o tempo de execucao do algoritmo
	timespec start, finish;
	double elapsed;

	clock_gettime(CLOCK_MONOTONIC, &start);
	std::ifstream input(argv[1], ios::binary);

	if(! input.good())
	{
		std::cout <<  "Could not open or find the image\n";
		return -1;
	}

	long int width, height;
	int mode, maxValue;

	int fileGood = fileOperations::readHeader(input, width, height, mode, maxValue);

	if(! fileGood)
	{
		std::cout <<  "File not properly formatted\n";
		return -1;
	}

	float max_memory_ratio = argc < 4 ? 0.9 : atof(argv[3]);


	while(max_memory_ratio >= 1.0 || max_memory_ratio <=0 )
	{
		std::cout <<  "Invalid max memory ratio, please insert a value in the interval (0,1): ";
		std:: cin >> max_memory_ratio;
	}

	// Leitura do conteudo da imagem
	unsigned char* image = fileOperations::readImage(input, width, height, mode);

	input.close();

	std::cout << "Altura total imagem: " << height << "\t Largura: " << width << "\n\n";

#ifdef _DO_CUDA_PROFILING
	cudaProfilerStart();
#endif

	size_t total_bytes, free_bytes;
	size_t total_size = width * height * sizeof(unsigned char);
	unsigned char *d_input, *d_output, *h_output_aux;

	// Verifica a quantidade de memoria disponivel e calcula o tamanho de cada bloco a ser enviado para GPU
	checkCuda(cudaMemGetInfo(&free_bytes, &total_bytes));

	unsigned int chunk_height = min((unsigned int)height, (unsigned int)(((free_bytes/2) * max_memory_ratio) / (width * sizeof(unsigned char))));
	unsigned int slices = chunk_height != height ? int((double)height/(double)chunk_height) + 1 : 1;

	size_t mem_size = width * (chunk_height + 4) * sizeof(unsigned char);
	size_t current_mem_size;
	
	// Aloca memoria de entrada e saida no dispositivo
	checkCuda(cudaMalloc(&d_input, mem_size));
	checkCuda(cudaMalloc(&d_output, mem_size));

	h_output_aux = new unsigned char[width * (chunk_height + 4)];

	std::cout << "Memoria total: " << total_bytes << " bytes\nMemoria disponivel: " << free_bytes << " bytes\nMemoria necessaria: "  << total_size << " bytes\nMemoria usada: " << mem_size*2 << "bytes\n";

	dim3 dimBlock(BLOCK_SIZE_X, BLOCK_SIZE_Y);
	unsigned char *pos;

	size_t prev_mem_size = 0;
	unsigned char * prev_pos;

	// Para cada canal de cor
	for(int i = 0; i < mode; i++)
	{
		std::cout << "Canal atual: " << i << "\n";
		pos = image + (width*height*i);

		int overlapSize = 2;
		int overlapTop = 0;
		int overlapBottom = slices > 1 ? overlapSize : 0;
		int prev_overlapTop;

		unsigned int current_chunk_height;
		std::cout << "Faixas: " << slices << "\n";
		
		// Para cada faixa
		for (int j = 0; j < slices; j++)
		{
			current_chunk_height = min((unsigned int)chunk_height, (unsigned int)(height - j * chunk_height));
			std::cout << "Altura da faixa: " << current_chunk_height << "\n";

			current_chunk_height = current_chunk_height + overlapTop + overlapBottom;
			current_mem_size = current_chunk_height * width * sizeof(unsigned char);
			dim3 dimGrid(int(width/dimBlock.x)+1, int(current_chunk_height/dimBlock.y)+1);

			checkCuda(cudaMemcpy(d_input, pos - overlapTop * width, current_mem_size, cudaMemcpyHostToDevice));

			if (prev_mem_size != 0)
			{
				memcpy(prev_pos, h_output_aux + prev_overlapTop * width, prev_mem_size - prev_overlapTop * width);
				prev_mem_size = 0;
			}

			d_avgFilterRGB<<<dimGrid, dimBlock>>>(d_input, d_output, mode, width, current_chunk_height);

			checkCuda(cudaMemcpy(h_output_aux, d_output, current_mem_size - overlapTop * width, cudaMemcpyDeviceToHost));

			prev_mem_size = current_mem_size;
			prev_pos = pos;
			prev_overlapTop = overlapTop;

			pos += (width * chunk_height);
			overlapTop = overlapSize;
			overlapBottom = j == slices - 2 ? 0 : overlapSize;
		}
		if (prev_mem_size != 0)
		{
			memcpy(prev_pos, h_output_aux + prev_overlapTop * width, prev_mem_size - prev_overlapTop * width);
		}
	}

	checkCuda(cudaFree(d_input));
	checkCuda(cudaFree(d_output));

#ifdef _DO_CUDA_PROFILING
	cudaProfilerStop();
#endif

	std::ofstream out(argv[2], ios::binary);

	// Escreve a saida
	fileOperations::writeOutput(out, width, height, mode, maxValue, image);

	out.close();

	delete[] image;
	delete[] h_output_aux;

	clock_gettime(CLOCK_MONOTONIC, &finish);

	// Calcula o tempo de execucao do algoritmo e imprime na tela
	elapsed = (finish.tv_sec - start.tv_sec);
	elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;

	std::cout << "Tempo de execucao do algoritmo: " << elapsed << "\n\n";

	return 0;
}

